# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Prioridad(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        db_table = 'prioridad'

class Sexo(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=10)

    class Meta:
        db_table = 'sexo'

class Estado(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        db_table = 'estado'

class TipoDocumento(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=3)

    class Meta:
        db_table = 'tipo_documento'

class Persona(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_documento = models.ForeignKey('TipoDocumento', db_column='tipo_documento', on_delete = models.CASCADE)
    nro_documento = models.CharField(unique=True, max_length=30)
    razon_social = models.CharField(max_length=32)
    telefono = models.CharField(max_length=13)
    direccion = models.CharField(max_length=50)
    email = models.CharField(max_length=32)
    sexo = models.ForeignKey('Sexo', db_column='sexo', blank=True, null=True, on_delete = models.CASCADE)

    class Meta:
        db_table = 'persona'

class Servicio(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=32)
    codigo = models.CharField(unique=True, max_length=3)

    class Meta:
        db_table = 'servicio'

class Usuario(models.Model):
    id = models.IntegerField(primary_key=True)
    persona = models.ForeignKey(Persona, db_column='persona', on_delete = models.CASCADE)
    cod_usuario = models.IntegerField(unique=True)
    contrasena = models.CharField(max_length=32)
    admin = models.BooleanField()

    class Meta:
        db_table = 'usuario'

class Box(models.Model):
    id = models.IntegerField(primary_key=True)
    numero = models.IntegerField()
    servicio = models.ForeignKey('Servicio', db_column='servicio', on_delete = models.CASCADE)
    usuario = models.ForeignKey('Usuario', db_column='usuario', on_delete = models.CASCADE)
    activo = models.BooleanField()

    class Meta:
        db_table = 'box'

class Cola(models.Model):
    id = models.IntegerField(primary_key=True)
    numero = models.IntegerField()
    servicio = models.ForeignKey('Servicio', db_column='servicio', on_delete = models.CASCADE)
    prioridad = models.ForeignKey('Prioridad', db_column='prioridad', on_delete = models.CASCADE)
    persona = models.ForeignKey('Persona', db_column='persona', on_delete = models.CASCADE)
    estado = models.ForeignKey('Estado', db_column='estado', on_delete = models.CASCADE)
    hora_entrada = models.DateTimeField()
    box = models.ForeignKey(Box, db_column='box', blank=True, null=True, on_delete = models.CASCADE)

    class Meta:
        db_table = 'cola'